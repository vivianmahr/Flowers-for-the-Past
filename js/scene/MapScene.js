// handled differently per game
define(["physics/Vector", "lib/goody", "scene/Scene", "map/FFTPMap" , "entities/Cursor", "entities/MainChar", "physics/CollisionHandler", "display/MapCamera",  "entities/Switch", "entities/SceneTransitionArea"],
function(Vector, goody, Scene, FFTPMap, Cursor, MainChar, CollisionHandler, MapCamera, Switch, SceneTransitionArea)
{    
    MapScene.prototype = new Scene.Scene();
    MapScene.prototype.constructor = MapScene;

    function MapScene(ctx, json) {
        this.map = new FFTPMap.FFTPMap(json);
        this.cursor = new Cursor.Cursor();
        this.MC = new MainChar.MainChar();
        this.nextScene = ""
        this.nextScenePosition = "";

        this.entities = [];
        this.overlayEntities = [this.cursor];
        this.stationary = [];
        this.moving = [];
        this.doors = [];
        this.map.sceneTransitions = [];

		var objects = this.map.objects;
        for (var i = 0; i < objects.length; i++) {
            if (objects[i].template === undefined) {
                // multiple part object
                if (goody.stringContains(objects[i].name, "SWITCH")) {
                    for (var n = 0; n < objects[i].objects.length; n++){
                        var o = objects[i].objects[n];
                        if (goody.stringContains(o.template, "switchFlower")) {
                            var s = new Switch.Switch(o.x, o.y);
                            this.entities.push(s);
                        }

                    }
                }
            }
            else if (goody.stringContains(objects[i].template, "exit")){
                var door = objects[i];
                var d = new SceneTransitionArea.SceneTransitionArea(door.x, door.y, door.width, door.height, door.properties);
                this.entities.push(d);
                this.doors.push(d);
            }
            else {   
                if (goody.stringContains(objects[i].template, "MCSpawn")) {
                    this.MC.setPosition(objects[i].x, objects[i].y);
                }
            }
        }
        this.entities.push(this.MC);
        this.moving.push(this.MC);

        this.collisionHandler = new CollisionHandler.CollisionHandler();
        this.camera = new MapCamera.MapCamera(ctx);
        this.camera.loadMap(this.map);
    }

    MapScene.prototype.update = function(input, delta) {
        this.MC.update(input, this.map, this.collisionHandler, delta);
        this.collisionHandler.collidingEntities(this.moving, this.stationary);
        this.collisionHandler.collidingEntities(this.moving, this.doors, this);
        this.cursor.update(input);
    }

    MapScene.prototype.click = function(mousePosition) {
    }

    MapScene.prototype.rightClick = function(mousePosition) {
    }

    MapScene.prototype.display = function() {

        this.camera.display(this.MC, [this.cursor], this.entities);
    }

    return {
        MapScene: MapScene
    };
});