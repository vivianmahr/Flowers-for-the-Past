define(["lib/goody"],
function(goody)
{    
    function CollisionHandler() {}

    CollisionHandler.prototype.collidingTiles = function(map, rect) {
        // Given a rect, find out which tiles it is on
        var points = rect.getCorners();
        // THIS HAS A CHANCE OF RETURNING THE SAME TILE TWICE 
        // IF HTE OBJECT IS SMALLER THAN A TILE
        var result = [];
        for (var i=0; i<4; i++) {
            var tile = map.pixelToTile(points[i]);
            if (!goody.inArray(result, tile)) {
                 result.push(tile);
            }
        }
        return result;
    }

    CollisionHandler.prototype.collidingEntities = function(moving, stationary, scene) {
        // stationary entities should never collide with each other, in a way, they 
        // are more like events
        for (var i = 0; i <  moving.length; i++) {
            for (var n = 0; n < stationary.length; n++){
                if (moving[i].rect.collideRect(stationary[n].rect)){
                    if (scene !== undefined) {
                        moving[i].collideWithEntity(stationary[n]);
                        stationary[n].collideWithEntity(moving[i], scene);
                    }
                    else {
                        moving[i].collideWithEntity(stationary[n]);
                        stationary[n].collideWithEntity(moving[i]);
                    }
                }
            }
        }
    }
    
    return {
        CollisionHandler: CollisionHandler
    };
});