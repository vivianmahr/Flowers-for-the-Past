define(["scene/MenuScene", "scene/MapScene", "input/InputHandler", "levels/maps", "assets/vars", "map/SceneHandler"],
function(MenuScene, MapScene, InputHandler, maps, vars, SceneHandler)
{
    function mainLoop() {
        this.canvas = document.getElementById('canvas');
        this.canvas.width = vars.displayWidth;
        this.canvas.height = vars.displayHeight;

        this.input = new InputHandler.InputHandler(); 
        this.ctx = this.canvas.getContext('2d');
        this.sceneHandler = new SceneHandler.SceneHandler();
        this.sceneHandler.loadScene(this.ctx, "debugSwitches")

        this.scene = this.sceneHandler.transitionScene();

        this.resizeCanvas();  
    };
    
    mainLoop.prototype.resizeCanvas = function() {
        this.canvas.style.marginTop = (window.innerHeight-vars.displayHeight)/2 + "px";
        this.draw();
    };
    
    mainLoop.prototype.updateInput = function(event) {   
        this.input.update(event, this.scene);
    }; 
    
    mainLoop.prototype.draw = function() {
        this.scene.display(this.ctx);
    };
    
    mainLoop.prototype.update = function(delta) {
        if (this.scene.switchScenes) {
            var x = this.scene.nextScenePosition.x;
            var y = this.scene.nextScenePosition.y;
            this.scene = new MapScene.MapScene(this.ctx, maps[this.scene.nextScene], 0);
            this.scene.MC.setPosition(x, y);
        }
        this.scene.update(this.input, delta);
    };
    
    return {
        mainLoop : mainLoop
    };
});