define(["display/Animation", "entities/Entity", "physics/Vector", "assets/vars"],
function(Animation, Entity, Vector, vars)
{    
    Switch.prototype = new Entity.Entity();
    Switch.prototype.constructor = Switch;

    function Switch(x, y, z) {
        Entity.Entity.apply(this, arguments);
        this.stationary = true;
        this.triggered = false;
        this._sprite = new Animation.Animation(images.plant_switch, 2, 24, 24);
        this.rect.width = 24;
        this.rect.height = 24;
    }

    Switch.prototype.update = function(input, map, collisionHandler, timeDelta) {
    }

    Switch.prototype.drawImage = function(ctx, offset) {
        this._sprite.display(ctx, new Vector.Vector(this.rect.position.x + offset.x - 3, this.rect.position.y + offset.y - 24))
        // this.rect.draw(ctx, offset);
    }

    Switch.prototype.collideWithEntity = function(entity) {
        if (!entity.stationary) {
        	if (!this.triggered){	
        		this._sprite.update();
        		this.triggered = true;
        	}
        }
    }

    return {
        Switch: Switch
    };
});