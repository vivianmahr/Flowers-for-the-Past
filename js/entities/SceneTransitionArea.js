define(["display/Animation", "entities/Entity", "physics/Vector", "assets/vars"],
function(Animation, Entity, Vector, vars)
{    
    SceneTransitionArea.prototype = new Entity.Entity();
    SceneTransitionArea.prototype.constructor = SceneTransitionArea;

    function SceneTransitionArea(x, y, width, height, transitionInfo) {
        Entity.Entity.apply(this, arguments);
        this.stationary = true;
        this.rect.width = width;
        this.rect.height = height;
        var x = -1;
        var y = -1;
        for (var i = 0; i < transitionInfo.length; i++) {
            if (transitionInfo[i].name === "transition"){
                this.destinationMap = transitionInfo[i].value;
            }
            else if (transitionInfo[i].name === "x_teleport") {
                x = transitionInfo[i].value;
            }
            else if (transitionInfo[i].name === "y_teleport") {
                y = transitionInfo[i].value;
            }

        }
        this.destination = new Vector.Vector(x, y);
    }

    SceneTransitionArea.prototype.update = function(input, map, collisionHandler, timeDelta) {
    }

    SceneTransitionArea.prototype.drawImage = function(ctx, offset) {
        this.rect.draw(ctx, offset);
    }

    SceneTransitionArea.prototype.collideWithEntity = function(entity, scene) {
        if (!entity.stationary && entity.triggersDoors) {
            scene.switchScenes = true;
            scene.nextScene = this.destinationMap;
            if (this.destination.x === -1) {
                this.destination.x = entity.rect.position.x;
            }
            if (this.destination.y === -1) {
                this.destination.y = entity.rect.position.y;
            }
            scene.nextScenePosition = this.destination;
        }
    }

    return {
        SceneTransitionArea: SceneTransitionArea
    };
});