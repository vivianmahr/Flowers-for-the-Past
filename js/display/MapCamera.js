define(["physics/Vector", "lib/goody", "assets/vars"],
function(Vector, goody, vars)
{    
    function MapCamera(ctx) {
        this._offset = new Vector.Vector(0, 0); 
        this._buffer = [];
        this._ctx = ctx;
        this._mapPixelWidth = 0;
        this._mapPixelHeight = 0;
        this._mapLength = 0;
        // Set font and color for debugging information
        this._ctx.font = "20px sans-serif";
        this._ctx.fillStyle = "#000000";
   }

    MapCamera.prototype.loadMap = function(map) {
        // Loads the buffer images for a map
        this._buffer = [];
        this._mapPixelWidth = map.pixelWidth;
        this._mapPixelHeight = map.pixelHeight;
        this._mapLength = map.length;
        var bufferLength = map.displayedLayers;

        // TODO
        if (map.parallax) {
            console.log(map);
            this.renderParallaxLayer("intro_P0");
        }

        for (var i = 0; i < bufferLength; i += 1) {
            this.renderLayer(map.imageMap[i], map, images.tileset_grass);
        }

    }

    MapCamera.prototype.renderParallaxLayer = function(parallaxImage) {
        // As of now, parallax cannot move, and the buffer is the size of
        //  the map, not forced to be the same size as the camera
        var i = this._buffer.length;
        this._buffer.push(document.createElement("canvas"));
        this._buffer[i].width = this._mapPixelWidth;
        this._buffer[i].height = this._mapPixelHeight;
        var ctx = this._buffer[i].getContext("2d");
        var image = images[parallaxImage];
        ctx.drawImage(
            image,                                                   //image
            0,                                                       //x position on image
            0,                                                       //y position on image
            image.width,                                             //imageWidth on Source
            image.height,                                            //imageHeight on Source
            0,                                                       //xPosCanvas    
            0,                                                       //yPosCanvas    
            image.width,                                             //imageWidth on Canvas
            image.height                                             //imageHeight on Canvas                
        );
    
    }

    MapCamera.prototype.renderLayer = function(layer, map, image) {
        // makes a context for the layer given, renders all the tiles on the context,
        // and adds it to the camera buffer.  
        var i = this._buffer.length;
        this._buffer.push(document.createElement("canvas"));
        this._buffer[i].width = this._mapPixelWidth;
        this._buffer[i].height = this._mapPixelHeight;
        var ctx = this._buffer[i].getContext("2d");
        for (var n = 0; n < this._mapLength; n++) {
            this.renderTile(n, layer[n], map, ctx, image);
        }
    }
    
    MapCamera.prototype._calcOffset = function(MC) {
        // Calculates the displacement of the map 
        var cwidth = vars.displayWidth;
        var cheight = vars.displayHeight;
        var MCpos = MC.rect.position;
        if (this._mapPixelWidth <= cwidth) {
            this._offset.x = (cwidth - this._mapPixelWidth)/2;
        } else {
            this._offset.x = Math.floor(goody.cap(cwidth / 2 - MCpos.x, -this._mapPixelWidth + cwidth, 0));        
        }
        if (this._mapPixelHeight <= cheight) {
            this._offset.y = (cheight - this._mapPixelHeight)/2;
        } else {
            this._offset.y = Math.floor(goody.cap(cheight / 2 - MCpos.y, -this._mapPixelHeight + cheight, 0));        
        }
    };

    MapCamera.prototype.showString = function(string, y) {
        // Displays a string on the upper left corner of the canvas
        this._ctx.fillText(string, 10, y);
    }

    //MapCamera.prototype.display = function(followingEntity, MC, cursor, objects) {
    MapCamera.prototype.display = function(followingEntity, UIEntities, objects) {
        this._calcOffset(followingEntity);

        // for maps that are smaller than the canvas
        this._ctx.fillRect(0, 0, canvas.width, canvas.height);

        // draw layers
        var bufferLength = this._buffer.length;
        for (var i = 0; i < bufferLength; i++) {
            this._ctx.drawImage(this._buffer[i], this._offset.x, this._offset.y);
        }
        objects.sort(function(a, b) { return a.rect.getBottom() - b.rect.getBottom();});
        for (var i = 0; i < objects.length; i++) {
            objects[i].drawImage(this._ctx, this._offset);
        }
        for (var i = 0; i < UIEntities.length; i++) {
            UIEntities[i].display(this._ctx, this._offset);
        }
    }

    MapCamera.prototype.absolutePosition = function(canvasPosition) {
        return new Vector.Vector(canvasPosition.x - this._offset.x, canvasPosition.y - this._offset.y);
    }
    
    MapCamera.prototype.renderTile = function(i, tile, map, ctx, image) {    

        var dim = vars.tileDimension;
        var mapVector = map.tileToPixel(i);
        // offset for the number and processing tiles
        //tile = tile - 13; 
        var xpos = ((tile-1) % (image.width / dim)) * dim;    
        var ypos = Math.floor((tile-1) / (image.width / dim)) * dim; 
        ctx.drawImage(
            image,                                                      //image
            xpos,                                                       //x position on image
            ypos,                                                       //y position on image
            dim,                                                        //imageWidth on Source
            dim,                                                        //imageHeight on Source
            mapVector.x,                                                //xPosCanvas    
            mapVector.y,                                                //yPosCanvas    
            dim,                                                        //imageWidth on Canvas
            dim                                                         //imageHeight on Canvas                
        );
    };
    
    return {
        MapCamera:MapCamera
    };
});