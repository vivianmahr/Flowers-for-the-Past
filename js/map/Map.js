// base class for maps used in games
define(["lib/goody", "physics/Vector", "assets/vars"],
function(goody, Vector, vars)
{    
    function Map(json){
        if (json !== undefined) {
            this.loadJSON(json);
        }
    };

    Map.prototype.loadJSON = function(json){
        this.height = json.height;
        this.width = json.width;
        this.pixelWidth = this.width * json.tileheight;
        this.pixelHeight = this.height * json.tilewidth;
        this.length = json.layers[0].data.length; // # of tiles in a map
        this.displayedLayers = 0;   // for mapCamera buffer, calculate in child
        this.parallax = false;
        this.imageMap = [];
    }

    // Generally helpful "get to adjacent tile/zone" functions
    Map.prototype.indexUp = function(n) { return n - this.width; };
    Map.prototype.indexDown = function(n) { return n + this.width; };
    Map.prototype.indexRight = function(n) { return n + 1; };
    Map.prototype.indexLeft = function(n) { return n - 1; };
    Map.prototype.indexUpRight = function(n) {return n - this.width + 1; };
    Map.prototype.indexUpLeft = function(n) {return n - this.width - 1; };
    Map.prototype.indexDownRight = function(n) {return n + this.width + 1; };
    Map.prototype.indexDownLeft = function(n) {return n + this.width - 1; };

    Map.prototype.findRow = function(tileNumber) { return Math.floor(tileNumber / this.width); }
    Map.prototype.findColumn = function(tileNumber) { return tileNumber % this.width; }

    Map.prototype.compareAdjacentTiles = function(tileElement, zone) {
        var result =  {
            "up" : !goody.arrayEquals(this.getZoneElement(this.zoneIndexUp(zone)), tileElement), 
            "upRight": !goody.arrayEquals(this.getZoneElement(this.zoneIndexUpRight(zone)), tileElement), 
            "right" : !goody.arrayEquals(this.getZoneElement(this.zoneIndexRight(zone)), tileElement), 
            "downRight" : !goody.arrayEquals(this.getZoneElement(this.zoneIndexDownRight(zone)), tileElement), 
            "down" : !goody.arrayEquals(this.getZoneElement(this.zoneIndexDown(zone)), tileElement), 
            "downLeft" : !goody.arrayEquals(this.getZoneElement(this.zoneIndexDownLeft(zone)), tileElement), 
            "left" : !goody.arrayEquals(this.getZoneElement(this.zoneIndexLeft(zone)), tileElement), 
            "upLeft" : !goody.arrayEquals(this.getZoneElement(this.zoneIndexUpLeft(zone)), tileElement),    
        }
        return result;
    }

    Map.prototype.findZonebyPixel = function(pixelVector) {
        return this.findZonebyIndex(this.pixelToTile(pixelVector));
    }

    Map.prototype.getHeight = function(tileIndex) {
        return this.heightMap[tileIndex];
    }


    Map.prototype.pixelToTile = function(point) {
        var column =  Math.floor(point.x/vars.tileDimension);
        var row = Math.floor(point.y/vars.tileDimension);
        return row * this.width + column;
    }
    
    Map.prototype.tileToPixel = function(tileNumber) { 
        return new Vector.Vector(tileNumber%this.width * vars.tileDimension, Math.floor(tileNumber/this.width) * vars.tileDimension); 
    };
        
    return {
        Map: Map
    };
});