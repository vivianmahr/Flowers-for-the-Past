define(["map/FFTPMap",  "physics/Vector", "levels/maps", "scene/MapScene"],
function(FFTPMap, Vector, maps, MapScene){
	// will need to be able to flex between mapscenes, menuscenes, and cutscenes later on
	// class that holds information about the new scene between transitions
	function SceneHandler() {
		this.transition = false;
		this.newScene = 0;
	}
	SceneHandler.prototype.setMCLocation = function(x, y) {
		if (this.newScene.hasMC) {
			this.MCLocation = new Vector(x, y);
		}
	}
	SceneHandler.prototype.loadScene = function(ctx, sceneName) {
		this.newScene = new MapScene.MapScene(ctx, maps[sceneName]);
		this.transition = true;
	}
	SceneHandler.prototype.transitionScene = function() {
		this.transition = false;
		return this.newScene;
	}
    return {
        SceneHandler: SceneHandler
    };
});