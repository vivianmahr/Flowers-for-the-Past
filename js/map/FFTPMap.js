define(["map/Map",  "lib/goody"],
function(Map, goody)
{    
    FFTPMap.prototype = new Map.Map();
    FFTPMap.prototype.constructor = FFTPMap;

    function FFTPMap(json) {
    	Map.Map.apply(this, arguments);

    	//Existing layer names - ENTITIES, COLLISION, BASE
        this.objects = [];
        this.collisionMap = [];
        this.sceneTransitions = [];
        this.hasMC = true;
        this.playerControlled = true;
        this.linked_objects = [];
        var layers = json.layers;

        for (var i = 0; i < layers.length; i++) {
            var name = layers[i].name;
			if (goody.stringContains(name, "BASE")) {
                this.imageMap.push(layers[i].data);
            }
            else if (goody.stringContains(name, "SWITCH")) {
                this.objects.push(layers[i]);
            }
            else if (name === "COLLISION") {
                this.collisionMap = layers[i].data;
            }
            else if (name === "MAP") {
                this.objects = this.objects.concat(layers[i].objects);
            }
            else if (name === "DOORS") {
            	this.sceneTransitions = layers[i].objects;
            }
            else {
            	console.log(name, "layer not found");
            };
        }
       	this.displayedLayers = this.imageMap.length;
    }
    return {
        FFTPMap: FFTPMap
    };
});