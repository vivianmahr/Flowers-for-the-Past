// Linked list, mainly for switch vine objects
define(["lib/goody"],
function(goody)
{    
    function LinkedList(value, next, previous) {
        this.value = value;
        this.next = goody.optional(next, 0);
        this.previous = goody.optional(next, 0);
    }

    LinkedList.prototype.last() {
        var toReturn = this;
        while (toReturn.next !== 0) {
            toReturn = toReturn.next;
        }
        return toReturn;
    }

    LinkedList.prototype.first() {
        var toReturn = this;
        while (toReturn.next !== 0) {
            toReturn = toReturn.next;
        }
        return toReturn;
    }
    
    return {
        LinkedList: LinkedList
    };
});